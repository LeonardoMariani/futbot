#include "BluetoothSerial.h" //Header File for Serial Bluetooth, will be added by default into Arduino
#include <Servo.h>
// #define SERVO 13
BluetoothSerial ESP_BT; //Object for Bluetooth
int incoming;
int statusLed = 0;

// Motor A
int in1 = 15; 
int in2 = 2; 
//Motor B
int in3 = 0; 
int in4 = 4; 

//Servo
Servo s; // Variável Servo
int pos; // Posição Servo

void setup() {
  Serial.begin(9600); //Start Serial monitor in 9600
  ESP_BT.begin("ESP32_LED"); //Name of your Bluetooth Signal
  Serial.println("Bluetooth Device is Ready to Pair");

  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);

  pinMode(in3, OUTPUT);
  pinMode(in4, OUTPUT);

  s.attach(12);
}

void loop() {
  if (ESP_BT.available()) //Check if we receive anything from Bluetooth
  {       
    incoming = ESP_BT.read(); //Read what we recevive 
    Serial.print("Received:"); Serial.println(incoming);

    if (incoming == 48){//chute	
      Serial.println("Chute");
      s.write(0);
      s.write(90);
      delay(500);
      s.write(0);
	
    }else if(incoming == 49){//cima
      Serial.println("cima");
      digitalWrite(in1, LOW);
      digitalWrite(in2, HIGH);
       
      digitalWrite(in3, LOW);
      digitalWrite(in4, HIGH);
	
	}else if(incoming == 50){//cima direita
      Serial.println("cima direita");
	
	}else if(incoming == 51){//direita
      Serial.println("direita");
      digitalWrite(in1, LOW);
      digitalWrite(in2, HIGH);
       
      digitalWrite(in3, LOW);
      digitalWrite(in4, LOW);
	
	}else if(incoming == 52){//baixo direita
      Serial.println("baixo direita");
	
	}else if(incoming == 53){//baixo
      Serial.println("baixo");
      digitalWrite(in1, HIGH);
      digitalWrite(in2, LOW);
       
      digitalWrite(in3, HIGH);
      digitalWrite(in4, LOW);
	
	}else if(incoming == 54){//baixo esquerda
      Serial.println("baixo esquerda");
	
	}else if(incoming == 55){//esquerda
      Serial.println("esquerda");
      digitalWrite(in1, LOW);
      digitalWrite(in2, LOW);
       
      digitalWrite(in3, LOW);
      digitalWrite(in4, HIGH);
	
	}else if(incoming == 56){//cima esquerda
      Serial.println("cima esquerda");
	
	}else if(incoming == 57){//Centro
      Serial.println("Centro");
      digitalWrite(in1, LOW);
      digitalWrite(in2, LOW);
       
      digitalWrite(in3, LOW);
      digitalWrite(in4, LOW);
      
	
	}       
  }
  delay(20);
}

