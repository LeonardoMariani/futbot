package com.example.user.futbot;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Comment;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    RelativeLayout layout_joystick;
    RelativeLayout layout_chute;
    ImageView image_joystick, image_border;
    TextView textView5;
    Button btnConectar;
    boolean conectado = false;
    JoyStickClass js;
    private final String DEVICE_ADDRESS="B4:E6:2D:F6:C4:37";  //mac bt esp32
    private final UUID PORT_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");//Serial Port Service ID
    private BluetoothDevice device;
    private BluetoothSocket socket;
    private OutputStream outputStream;
    private InputStream inputStream;
    boolean deviceConnected=false;
    Thread thread;
    byte buffer[] = null;
    int bufferPosition;
    boolean stopThread;
    private String readMessage = null;
    private String rotulo = null;
    private boolean flag = false;
    private final static int MESSAGE_READ = 2;
    boolean bleOk = false;
	private int movimento = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponentes();
    }
    private boolean BTinit() {
        boolean found=false;
        BluetoothAdapter bluetoothAdapter=BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(),"Device doesnt Support Bluetooth",Toast.LENGTH_SHORT).show();
        }
        if(!bluetoothAdapter.isEnabled())
        {
            Intent enableAdapter = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableAdapter, 0);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
        if(bondedDevices.isEmpty())
        {
            Toast.makeText(getApplicationContext(),"Please Pair the Device first",Toast.LENGTH_SHORT).show();
        }
        else
        {
            for (BluetoothDevice iterator : bondedDevices)
            {
                if(iterator.getAddress().equals(DEVICE_ADDRESS))
                {
                    device=iterator;
                    found=true;
                    break;
                }
            }
        }
        return found;
    }
    private boolean BTconnect() {
        boolean connected=true;
        try {
            socket = device.createInsecureRfcommSocketToServiceRecord(PORT_UUID);
            //socket = device.createRfcommSocketToServiceRecord(PORT_UUID);
            socket.connect();
        } catch (IOException e) {
            e.printStackTrace();
            connected=false;
        }
        if(connected)
        {
            try {
                outputStream=socket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                inputStream=socket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        return connected;
    }
    private void beginListenForData() {
        final Handler handler = new Handler();
        stopThread = false;
        buffer = new byte[1024];
        Thread thread  = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopThread)
                {
                    try
                    {
                        int byteCount = inputStream.available();
                        if(byteCount > 0)
                        {
                            byte[] rawBytes = new byte[byteCount];
                            inputStream.read(rawBytes);
                            final String string=new String(rawBytes,"UTF-8");
                            handler.post(new Runnable() {
                                public void run()
                                {
                                    //textView.append(string);
                                }
                            });

                        }
                    }
                    catch (IOException ex)
                    {
                        stopThread = true;
                    }
                }
            }
        });

        thread.start();
    }

    private boolean initBLE() {
        boolean initOk = false;

        if(BTinit())
            if(BTconnect())
                initOk = true;
                deviceConnected=true;
                beginListenForData();
        return initOk;

    }

    public void initComponentes(){
        textView5 = (TextView)findViewById(R.id.textView5);
        btnConectar = findViewById(R.id.buttonConectar);
        layout_joystick = (RelativeLayout)findViewById(R.id.layout_joystick);
        layout_chute = findViewById(R.id.layout_chute);

        js = new JoyStickClass(getApplicationContext()
                , layout_joystick, R.drawable.image_button);
        js.setStickSize(150, 150);
        js.setLayoutSize(500, 500);
        js.setLayoutAlpha(150);
        js.setStickAlpha(100);
        js.setOffset(90);
        js.setMinimumDistance(50);

        layout_chute.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_UP){
                    Toast.makeText(MainActivity.this,"Chute", Toast.LENGTH_LONG).show();
                    chutar();
                }
                return true;
            }
        });

        layout_joystick.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                js.drawStick(arg1);
                if(arg1.getAction() == MotionEvent.ACTION_DOWN
                        || arg1.getAction() == MotionEvent.ACTION_MOVE) {

                    int direction = js.get8Direction();
                    if(direction == JoyStickClass.STICK_UP) {
                        textView5.setText("Direção : cima");
                        if(movimento != 1) {
                            movimentar("1");
                            movimento = 1;
                        }
                    } else if(direction == JoyStickClass.STICK_UPRIGHT) {
                        textView5.setText("Direção : cima direita");
                        if(movimento != 2) {
                            movimentar("2");
                            movimento = 2;
                        }
                    } else if(direction == JoyStickClass.STICK_RIGHT) {
                        textView5.setText("Direção : direita");
                        if(movimento != 3) {
                            movimentar("3");
                            movimento = 3;
                        }
                    } else if(direction == JoyStickClass.STICK_DOWNRIGHT) {
                        textView5.setText("Direção : baixo direita");
                        if(movimento != 4) {
                            movimentar("4");
                            movimento = 4;
                        }
                    } else if(direction == JoyStickClass.STICK_DOWN) {
                        textView5.setText("Direção : baixo");
                        if(movimento != 5) {
                            movimentar("5");
                            movimento = 5;
                        }
                    } else if(direction == JoyStickClass.STICK_DOWNLEFT) {
                        textView5.setText("Direção : baixo esquerda");
                        if(movimento != 6) {
                            movimentar("6");
                            movimento = 6;
                        }
                    } else if(direction == JoyStickClass.STICK_LEFT) {
                        textView5.setText("Direção : esquerda");
                        if(movimento != 7) {
                            movimentar("7");
                            movimento = 7;
                        }
                    } else if(direction == JoyStickClass.STICK_UPLEFT) {
                        textView5.setText("Direção : cima esquerda");
                        if(movimento != 8) {
                            movimentar("8");
                            movimento = 8;
                        }
                    } else if(direction == JoyStickClass.STICK_NONE) {
                        textView5.setText("Direção : centro");
                        if(movimento != 9) {
                            movimentar("9");
                            movimento = 9;
                        }
                    }
                } else if(arg1.getAction() == MotionEvent.ACTION_UP) {
                    textView5.setText("Direção :");
                }
                return true;
            }
        });

    }
	
	private void movimentar(String move){	       
        move.concat("\n");
        try{
            outputStream.write(move.getBytes());

        }catch (Exception e){
            e.printStackTrace();
        }	
	}
    private void chutar() {

        String string = "0";
        string.concat("\n");
        try{
            outputStream.write(string.getBytes());

        }catch (Exception e){
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(),"Chutou",Toast.LENGTH_SHORT).show();
    }
    public void fechar() throws IOException {
        if(stopThread == false){
            stopThread = true;
        }
        outputStream.close();
        inputStream.close();
        socket.close();
        deviceConnected=false;
    }

    public void conectar(View view) {

        bleOk = initBLE();

        if(bleOk){
            Toast.makeText(MainActivity.this,"BLE OK", Toast.LENGTH_LONG).show();
            btnConectar.setEnabled(false);
        }else
            Toast.makeText(MainActivity.this,"BLE DEU RUIM !!!", Toast.LENGTH_LONG).show();

    }
}
